# Why the smartphone is not the end-game mobile device

2014-10-27

Smartphones aren't just a fad, but they are just a phase.

I'm not writing to shake an impotent fist against *youths* or conflabbing
dang-busted newfangled tarnations or whatever. Pocket-size computers are
obviously super convenient, so it's little wonder that they've grown so popular
so quickly. But I hope we all recognize that this isn't the pinnacle of
evolution for mobile computing.

The effectiveness of an image is based partly on its aesthetics, of course, but
also depends significantly on its visual size.

<img class="aligncenter size-full wp-image-255" src="https://foxcoloredtrees.files.wordpress.com/2014/10/a-heat-map-of-the-emotional-impact-of-an-image1.jpg" alt="A heat map of the emotional impact of an image." width="388" height="387" />



A relatively mediocre image will have a much stronger emotional impact on a
viewer if it's rendered at a large size. And even the most magnificent painting
can be underwhelming when the thumbnail version is shrunk down to the size of
your actual thumbnail. The recent development of ultra-high resolution screens
on newer devices can help add clarity, but the power of an image I'm talking
about isn't tied to pixel density so much as surface area. No matter how well
designed your application or your infographic or your web comic or photo
portfolio may be, your visual media will never have nearly as strong an impact
on your users, viewers, and readers as the media will on a larger screen. Small
screens also greatly limit the *kinds* of designs you can use.

My graph is completely nonempirical, but there *is* real data about this
principle...

<ul>
	<li><a href="http://persuasivevideo.com/wp-content/uploads/2009/12/Reevesetall1999.pdf">The Effects of Screen Size and Message Content on Attention and Arousal</a>, by Reeves, Lang, Kim, and Tatar, published in 1999 by <cite>Media Psychology</cite>.</li>
	<li><a href="http://www.researchgate.net/publication/6253511_Arousal_and_attention_picture_size_and_emotional_reactions/file/d912f50b5bcdc60ca8.pdf">Arousal and attention: Picture size and emotional reactions</a>, by Codispoti and De Cesarei, published in 2007 in <cite>Psychophysiology</cite>.</li>
</ul>

This isn't to say that visual media on smartphones can't have an impact, because
obviously they can. Flappy Bird's obsessive players seem to be engaging pretty
deeply with the game. The many engineers designing smartphone and tablet
hardware and operating systems and interfaces have done well to make the best of
the form factor. But a mobile device's convenience and usefulness and *mobility*
are dependent on it being smaller, easier to carry -- which puts the technology
at odds with another trend in web culture of using ever richer multimedia. On
that note, then, the recent smartwatches from Google and Apple are even worse.

As a storyteller who uses images to help convey the narrative, I'm always a
little dismayed to hear the latest statistics again reaffirming that web users
everywhere are more and more using their smart phones instead of other devices
for just about everything. For one thing, it's obnoxious having to re-optimize
every piece of work I do for every conceivable display size. But also it means
that a significant portion of my audience isn't getting the full effect.

[caption id="attachment_254" align="aligncenter" width="388"]<img class="wp-image-254 size-full" src="https://foxcoloredtrees.files.wordpress.com/2014/10/futurama-eyephone.jpg" alt="Maybe things will be better when the eyePhone comes out." width="388" height="218" /> Maybe things will be better when the eyePhone comes out.[/caption]
