# Are you a feminist?

"Feminism" is a term with an unusually high amount of baggage associated
with it, so maybe it will be useful to first unpack the terminology a
bit.

I tend to see political labels like this as broad, fuzzy descriptive
terms, rather than sharply defined allegiances. For example, unless
you're a political scientist, "liberal" and "conservative" just refer to
two different circles in a Venn diagram, wherein people tend to answer
"yes" or "no" to the majority of a range of policy questions. You don't
have to agree with everything everyone who identifies with a political
label has ever said for that descriptor to still be accurate about you.

“Feminism is the radical idea that women are people too” \
-- Cheris Kamarae and Paula Treichler

“I myself have never been able to find out precisely what a feminist
is…I only know that I am called a feminist when I express sentiments
that distinguish me from a doormat, or a prostitute” \
-- Rebecca West

It doesn't have to be much more complicated than that. You believe that
people are often treated unfarily, unjustly, because of their gender,
and wish to create a more fair and just situation for everyone.
Everything beyond that is just implementation details. Seriously, the
movement is too big and broad to define any more specifically than that
— and it doesn't need to be any more specific, because political labels
are just a convenient shorthand anyway. If you want to talk about
specific policies, you have to name and discuss the policy itself.

Some prefer to call themselves "egalitarian," pointing out that the name
of feminism suggests something narrower than the broader appeal to
justice for people of various sexual orientations and gender identities.
Or the fact that men actually suffer under our current patriarchal
gender roles too, that many men are also stifled under the confines of a
toxic definition of masculinity. Our standard gender roles are bad for
everyone. I think I'm on board with this egalitarianism idea.

Some claim that people who identify as egalitarians tend to use their
preferred -ism as a way of sidetracking the discussion, of distracting
from or denying that women still tend to have it worse. I have not any
personal experience with this sort of argument, but since, again, these
are just descriptive terms, and everyone is a complex fractal of Venn
diagrams of opinions, I have no issue with saying that I am a feminist
too. And you don't have to be scared to admit it either.
