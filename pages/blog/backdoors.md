# Two Steps Backward: Breaking computer security will make us less secure (duh)

<figure>
	<img src="/illustrations/backdoor--by-eff.jpg" alt="" />
	<figcaption class="attribution">
		<a href="https://www.flickr.com/photos/electronicfrontierfoundation/14369529746/" rel="cite">
			backdoor
		</a>
		by
		<a href="https://www.eff.org" rel="cite">the EFF</a>
</figure>

In February, the FBI inadvertently made headlines when they ordered Apple to
build a backdoor into their iOS operating system. This would have allowed the
FBI full access to any iOS product at will. To the federal investigators, this
must have made perfect sense: They can get a warrant to scour a suspect's home
and read any handwritten notes and messages they find, so why should it be any
harder to get to the digital notes and messages that a suspect keeps on their
phone? Apple correctly and publicly disagreed.

Part of what made this story noteworthy was its very publicity --- orders like
the one Apple received normally also require secrecy, but CEO Tim Cook responded
with an [open letter](https://www.apple.com/customer-letter/). This wasn't the
first time a federal agency has ordered privileged access to user data, though.
The American digital surveillance capacity is
[large](http://www.nytimes.com/2013/06/20/technology/silicon-valley-and-spy-agency-bound-by-strengthening-web.html)
and [complex](https://www.theguardian.com/world/2013/jul/11/microsoft-nsa-collaboration-user-data),
and other nations are following the USA's example. More recently, the UK
Parliament passed a law,
[the Investigatory Powers Act](http://www.wired.co.uk/article/ip-bill-law-details-passed),
*mandating* that software developers create a backdoor if the UK government ever
asks. Other governments, including the normally privacy-conscious Germany, are
on their way to do the same.

For the moment, I'm not interested in arguing the ethics here. Instead, it's
important for you to know that these laws are objectively, factually,
*technologically* a Bad Idea™. Building these backdoors puts us all in
substantially *more* danger, not less.

The defense that government officials always trot out is that these are
backdoors only for government use --- and therefore, if you aren't doing
anything wrong, you have nothing to fear, etc, etc. However, it's literally
impossible to ensure that these backdoors will only ever be used for
"legitimate" government-led investigations. One of the most important principles
of digital security is that any piece of data, any device, any system that can
be accessed for legitimate purposes by its owner can also potentially be
accessed by illegitimate third parties. All login systems can crack, all memory
can leak, all private databases can go public, all devices can be lost or
stolen, and all secret backdoors can be discovered. Most of these are risks that
we must simply live with to take advantage of the wonders of the Digital Age.
The critical difference here is that when a hacker guesses or decrypts a
password, they only gain access to a single login account. When a hacker runs a
SQL injection attack, they still can only access the affected database. But when
an attacker finds a way into a backdoor, they gain total access to every single
system where the backdoor has been implemented.

And that's definitely <i>when</i>, not <i>if</i>. Think of it like this: If you
wanted to protect your rarest Pokémon cards, you would not put them in a safe
and leave it with a Pokémon-loving thief. Obviously the thief will find a way to
get in there eventually, and then your mint condition 1998 holographic Pikachu
Illustrator card is gone. When you build backdoors into a piece of software, you
basically put *everyone's* Pokémon in the same safe, and leave it where all the
thieves can poke and prod it.

<figure>
	<img src="/screen-captures/team-rocket-steals-pikachu.gif" alt="" />
</figure>

The governments of the world all want a skeleton key to our digital homes. It
doesn't matter if they promise not to give the key away or promise to only use
the key responsibly, because every hacker will be living with the same backdoor
in their homes too, and they'll have all the time in the world to figure out
where it is and how it works. *When* they do, there's basically no limit to the
harm they can cause. Situations like
[this car hack](https://www.youtube.com/watch?v=MK0SrxBC1xs "Andy Greenberg of WIRED Magazine talks to NSA investigators who demonstrate ways they can remotely control a Jeep — with Andy still in it.")
would be just the beginning.
