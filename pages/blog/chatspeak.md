The English language is degrading -- and always has been...

> We are diminishing our ability to communicate with precision, value,
> meaning, and intelligence.

> -- a friend of a friend, on a social networking site

The idea that English even has rules is only a half-truth, I think.
There are so many exceptions and variations to every rule that people
learning it as their second language often feel they have to learn the
form of the language on a case-by-case basis rather than by practicing
the application of consistent principles. And the rules change depending
on the context, the intended message and audience. The fact of the
matter is that sometimes your choice and placement of punctuation is
often a matter of flavor and preference, because there is more than one
acceptable combination; you may consider, for example, the debates
people continue to have over the so-called Oxford Comma.

It's not that technology companies, from AOL to Zynga, are destroiyng or
perverting modern language -- they're creating a new language.
