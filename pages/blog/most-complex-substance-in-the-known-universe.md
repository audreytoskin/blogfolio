# The Most Complex Substance In the Known Universe

2013-08-10

I love brains and computers --- minds and machines. Maybe that seems like an odd
pairing, once you get past the alliteration? But the two
[go together](http://www.nature.com/nature/focus/brain/) a lot. Each is even
often used as a metaphorical framework for understanding the other: Perhaps
you've heard someone say their computer is "thinking about it" when it runs
slow. And societies have long compared the brain to the most sophisticated
technologies developed in their time --- it used to be thought that the brain
was like a musical instrument,  then that it was like a clock (which I imagine
is where expressions about "[seeing the cogs turning](http://www.goodreads.com/quotes/52983-the-duke-had-a-mind-that-ticked-like-a-clock)"
in someone's head come from), and now we tend to think of the brain being like a
computer. It's still an imperfect simile, but there's a little more evidence
supporting this latest conceptualization: Both are powerful information
processors.

And both are at least a little enigmatic to most people, I think. I'll speak for
myself here: For all the time I spend using them, computers are sort of a
mystery to me. I understand computers better than some; I can write some code
and I can troubleshoot (or at least Google my way out of) most problems I run
into. But I'm only being a little facetious when I run into a problem and I
shrug and say the gnomes are on strike. For all I know, that could be it. The
tiny gnomes listening to my keyboard and hastily painting pictures on the screen
got tired of the long hours and decided to band together and demand better
working conditions. No matter how many times I read about transistors, I have a
hard time wrapping my brain around the transition from ones and zeros, from
<em>bits</em> to, say, nonlinear video editing software.

Likewise with brains --- we use them all our lives, yet we're still often
impressed with what they're capable of achieving, and know very little about
how, specifically, they work. You probably know a little about neurons and
synapses, at least from middle school biology classes, but even the experts
studying this stuff can't fully explain the leap from action potentials to
consciousness, or emotions, or art. The human brain has
[more synapses than there are stars in the Milky Way](http://science.education.nih.gov/supplements/nih2/addiction/guide/lesson2-1.htm),
by some estimates, so there are a staggering number of little pieces in this
puzzle. The seemingly impossible gap between the brain and the mind is the
origin of [dualism](https://en.wikipedia.org/wiki/Dualism). The mystery as a
whole continues to elude even those who spend their entire careers picking apart
the smaller mysteries contained within.

Which, of course, is part of the fun.
