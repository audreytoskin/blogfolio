// Log error if window is on error
window.onerror = function(msg, url, line, col, error) {

    if(msg.indexOf('NetworkError') !== -1) {
        return;
    }

    var x = new XMLHttpRequest();
    x.open('POST', configuration.apiUrl + 'action/error');

    var params = [
        'product=search',
        'url=' + encodeURIComponent(url),
        'msg=' + msg,
        'line=' + line
    ];

    if (typeof col !== 'undefined') {
        params.push('col=' + col);
    }

    // Send the stack trace if it is provided
    if (typeof error !== 'undefined' && error !== null && error.stack) {
        params.push('stack=' + encodeURIComponent(error.stack));
    }

    x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    x.send(params.join('&'));

    // Only print errors in console on dev env
    return !configuration.isDevelopmentMode;
};

constants.AvailableLanguages = config_get('project.interface.languages');
constants.AvailableRegions   = config_get('project.regionalisation');

var detectedLanguage   = config_get('features.l10n.lang.default');
var preferredRegion    = config_get('features.l10n.region.default');
var preferredContinent = config_get('features.l10n.continent.default');

var browserLanguage = navigator.language || navigator.userLanguage;
var langMatch = Object.keys(constants.AvailableLanguages)
  .filter(function (key) {
    // Reduce the array of available languages keys to only those that
    // match the browser language
    return browserLanguage.indexOf(constants.AvailableLanguages[key].shorthand) == 0;
  });
if (langMatch && langMatch.length > 0) {
  // Take the first matched language from the filtered array
  detectedLanguage = langMatch.shift();
}
var splitTerritory = detectedTerritory.split('_');
preferredRegion = (LanguageHelper.getCountry(splitTerritory[1])) ? LanguageHelper.getCountry(splitTerritory[1]).code : preferredRegion;
preferredContinent = LanguageHelper.getContinentFromCountry(splitTerritory[1]);


/*
 * Detect whether or not localStorage is enabled
 */
var isReachableStore = true;
try {
  if (localStorage.getItem('user')) {
    var user = JSON.parse(localStorage.getItem('user'));
    if (user.userSetting && user.userSetting.interfaceLanguageKey) {
        detectedLanguage = user.userSetting.interfaceLanguageKey;
    }
  }
} catch (e) {
  isReachableStore = false;
}


// -- Begin URL settings parser -- //
/*
 * parseUri 1.2.2
 * (c) Steven Levithan <stevenlevithan.com>
 * MIT License
 */
function parseUri (str) {
    var o   = {
            strictMode: false,
            key       : ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
            q         : {
                name  : "queryKey",
                parser: /(?:^|&)([^&=]*)=?([^&]*)/g
            },
            parser    : {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose : /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
            }
        },
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) {
        uri[o.key[i]] = m[i] || "";
    }

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) {
            uri[o.q.name][$1] = $2;
        }
    });

    return uri;
}

var paramCallbacks = {},
    urlParams      = parseUri(window.location).queryKey,
    forcedLanguage = false;

if (Object.keys(urlParams).length > 0) {
    /*
     * Define the callbacks associated with the URL parameters
     */

    // Callback for the 'l' parameter
    paramCallbacks.l = function (locale) {
        if (!config_get('features.l10n.lang.switch.enabled')) {
            return false;
        }
        for (var lang in constants.AvailableLanguages) {
            if (constants.AvailableLanguages[lang].shorthand === locale) {
                forcedLanguage   = constants.AvailableLanguages[lang];
                detectedLanguage = forcedLanguage.code;
                break;
            }
        }
    };

    /*
     * Loop over the URL parameters and execute the
     * corresponding callbacks
     */
    for (var urlParam in urlParams) {
        // For each URL parameter, check to see if an associated callback exists
        for (var paramCallback in paramCallbacks) {
            // If the corresponding callback exists, execute it with the value of the URL parameter
            if (urlParam === paramCallback) {
                paramCallbacks[paramCallback](urlParams[urlParam]);
            }
        }
    }
}
// -- end URL settings parser -- //

/*
 * Initialize logger
 */
var l = Logger.getLogger(config_get('isDevelopmentMode') ? 'development' : 'production');
_initI18n({showMissingTranslation: config_get('isDevelopmentMode') && detectedLanguage !== 'en_gb'});

var languageFile = document.createElement('script');

// {"fr_fr":"/js/lang/fr_fr.js?6863fa1ed2ff301dcfc63ede36f472d3b0961d74","de_de":"/js/lang/de_de.js?c636e7036790aed6866fb8b6198e048ef4f52187","en_gb":"/js/lang/en_gb.js?940057253a154c543bd79cdeb800bae7fe22e02c","co_fr":"/js/lang/co_fr.js?6ac7c4b91b5e84fd300b9e9f65da1821020fa4be","br_fr":"/js/lang/br_fr.js?03725847998d5a19cd54f3f5a02f34a47d46287e","es_es":"/js/lang/es_es.js?0998140bfa14babb263648f84634d433164e06fc","eu_es":"/js/lang/eu_es.js?4608846d42ef3a6da7bc60c757dfcf5340da2cd4","ca_es":"/js/lang/ca_es.js?53e7ec1de4584eb2e68e423c8c42fc2fd63a7226","it_it":"/js/lang/it_it.js?b4cb7a8203dab813999e38b434f76360c5dff5ca","pt_pt":"/js/lang/pt_pt.js?0a2a627fcf969bb9d7199cf591d2e29139b2e8bf","nl_nl":"/js/lang/nl_nl.js?476f17ed87b35161f770daf90f1610e23e2b3e50","ru_ru":"/js/lang/ru_ru.js?80f4f6dd60dc61721de59798d5e036c26d826947","pl_pl":"/js/lang/pl_pl.js?94555d158f567282f0451f8dcfc2af954144725f"} is generated during build
// Because the filename of each language file is required in order to append the <checksum> component, the list is
// generated during build and gets substituted to {"fr_fr":"/js/lang/fr_fr.js?6863fa1ed2ff301dcfc63ede36f472d3b0961d74","de_de":"/js/lang/de_de.js?c636e7036790aed6866fb8b6198e048ef4f52187","en_gb":"/js/lang/en_gb.js?940057253a154c543bd79cdeb800bae7fe22e02c","co_fr":"/js/lang/co_fr.js?6ac7c4b91b5e84fd300b9e9f65da1821020fa4be","br_fr":"/js/lang/br_fr.js?03725847998d5a19cd54f3f5a02f34a47d46287e","es_es":"/js/lang/es_es.js?0998140bfa14babb263648f84634d433164e06fc","eu_es":"/js/lang/eu_es.js?4608846d42ef3a6da7bc60c757dfcf5340da2cd4","ca_es":"/js/lang/ca_es.js?53e7ec1de4584eb2e68e423c8c42fc2fd63a7226","it_it":"/js/lang/it_it.js?b4cb7a8203dab813999e38b434f76360c5dff5ca","pt_pt":"/js/lang/pt_pt.js?0a2a627fcf969bb9d7199cf591d2e29139b2e8bf","nl_nl":"/js/lang/nl_nl.js?476f17ed87b35161f770daf90f1610e23e2b3e50","ru_ru":"/js/lang/ru_ru.js?80f4f6dd60dc61721de59798d5e036c26d826947","pl_pl":"/js/lang/pl_pl.js?94555d158f567282f0451f8dcfc2af954144725f"}.
// see: #1701102123JB
// noinspection JSUnresolvedVariable
languageFile.src = {"fr_fr":"/js/lang/fr_fr.js?6863fa1ed2ff301dcfc63ede36f472d3b0961d74","de_de":"/js/lang/de_de.js?c636e7036790aed6866fb8b6198e048ef4f52187","en_gb":"/js/lang/en_gb.js?940057253a154c543bd79cdeb800bae7fe22e02c","co_fr":"/js/lang/co_fr.js?6ac7c4b91b5e84fd300b9e9f65da1821020fa4be","br_fr":"/js/lang/br_fr.js?03725847998d5a19cd54f3f5a02f34a47d46287e","es_es":"/js/lang/es_es.js?0998140bfa14babb263648f84634d433164e06fc","eu_es":"/js/lang/eu_es.js?4608846d42ef3a6da7bc60c757dfcf5340da2cd4","ca_es":"/js/lang/ca_es.js?53e7ec1de4584eb2e68e423c8c42fc2fd63a7226","it_it":"/js/lang/it_it.js?b4cb7a8203dab813999e38b434f76360c5dff5ca","pt_pt":"/js/lang/pt_pt.js?0a2a627fcf969bb9d7199cf591d2e29139b2e8bf","nl_nl":"/js/lang/nl_nl.js?476f17ed87b35161f770daf90f1610e23e2b3e50","ru_ru":"/js/lang/ru_ru.js?80f4f6dd60dc61721de59798d5e036c26d826947","pl_pl":"/js/lang/pl_pl.js?94555d158f567282f0451f8dcfc2af954144725f"}[detectedLanguage];
document.documentElement.setAttribute("lang", detectedLanguage.substr(0, 2));

var applicationState = null;

function initApplication () {
    Model.setup(false);
    var userPromise = SiteHelper.getUserModel()
        .getUser();

    userPromise
        .then(function (user) {
            return startApplication(user);
        })
        .then(function (error, needsRefresh) {
            // Refresh the application if we need to force an interface language
            if (needsRefresh) {
                applicationState.user.update();
                Application.rootComponent.refresh();
            }

            // Notify the user if their browser will soon be deprecated
            if (soonToBeDeprecatedBrowser) {
                AlertComponent.info(_('You are using an outdated and potentially unsecure browser. As of 3/13/2017, your browser will no longer be supported by Qwant.<br/>Please consider using <a href="{f4qUrl}">Firefox for Qwant</a>, or any other up-to-date browser. If you absolutely need to use this browser, you can use <a href="{liteUrl}">lite.qwant.com</a>.',
                    'homepage',
                    {f4qUrl: '/firefoxqwant/download', liteUrl: config_get('liteUrl')}
                ));
            }
        });
}

function startApplication (user) {
    applicationState = {
        rootUrl         : window.location.protocol + '//' + window.location.host,
        user            : user,
        timeMillisOffset: serverTime - new Date().getTime()
    };

    if (!user.isLogged && (detectedLanguage === "de_de" || detectedTerritory === "de_DE")) {
        user.userSetting.searchMode = window.siteConfiguration.searchCategories.WEB.index;
    }

    var application = new Application(new AppComponent(window.location.pathname), document.getElementById('app'));

    if (Dispatcher.route.params["client"]) {
        AjaxParameters.putPrefix("client", Dispatcher.route.params["client"])
    }

    var appStateInit = new promise.Promise();
    if (forcedLanguage) {
        appStateInit = applicationState.user.updateLang(forcedLanguage);
    } else {
        appStateInit.done(false, false);
    }
    return appStateInit;
}

languageFile.onerror = function () {
    document.body.removeChild(languageFile);
    var defaultLanguageFile    = document.createElement('script');
    defaultLanguageFile.src    = '/js/lang/' + config_get('features.l10n.lang.default') + '.js?1495207248901';
    defaultLanguageFile.onload = languageFileLoad;
    document.body.appendChild(defaultLanguageFile);
};

languageFile.onload = languageFileLoad;

function languageFileLoad () {
    try {
        var userExt = JSON.parse(localStorage.getItem("userExtension"));
    } catch (e) {
        userExt = null;
    }
    if (userExt !== null) {
        userExt.source = "extension";
        AjaxManager.post(ajax.user.extensionLogin(), new AjaxParameters(userExt).getParameters())
            .then(function (error, data) {
                if (!error && data.status === "success") {
                    var userModel = new UserModel(JSON.stringify(data));
                    return startApplication(userModel)
                        .then(function (error, needsRefresh) {
                            // Refresh the application if we need to force an interface language
                            if (needsRefresh) {
                                applicationState.user.update();
                                Application.rootComponent.refresh();
                            }
                        });
                } else {
                    try {
                        localStorage.removeItem("userExtension");
                    } catch (e) {}
                    initApplication();
                }
            });
    } else {
        initApplication();
    }
}

document.body.appendChild(languageFile);

document.addEventListener("qwant_extension_login", function () {
    window.location.reload();
});

document.addEventListener("qwant_extension_logout", function () {
    window.location.reload();
});